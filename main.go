package main

import (
	"agz/struct-practise-test/note"
	"agz/struct-practise-test/todo"
	"bufio"
	"fmt"
	"os"
	"strings"
)

func getNoteData() (string, string) {
	title := getUserInput("Note Title:")

	content := getUserInput("Note Content:")

	return title, content
}

type saver interface {
	Save() error
}

type displayer interface {
	Display()
}

type outputtable interface {
	displayer
	saver
}

func saveData(data saver) error {
	error := data.Save()

	if error != nil {
		fmt.Println("Saving the data failed.")
		return error
	}

	fmt.Println("Data saved successfully")
	return nil
}

func outputData(data outputtable) error {
	data.Display()
	return saveData(data)
}

func main() {
	printSomething(1)
	printSomething(1.4)
	printSomething("a, b, ")
	title, content := getNoteData()

	todoText := getUserInput("Todo Text")

	todo, error := todo.New(todoText)

	if error != nil {
		fmt.Println(error)
		return
	}

	userNote, error := note.New(title, content)

	if error != nil {
		fmt.Println(error)
		return
	}

	error = outputData(todo)

	if error != nil {

		return
	}

	outputData(userNote)

}

func getUserInput(prompt string) string {
	fmt.Printf("%v ", prompt)

	reader := bufio.NewReader(os.Stdin)

	text, error := reader.ReadString('\n')

	if error != nil {
		return ""
	}

	text = strings.TrimSuffix(text, "\n")
	text = strings.TrimSuffix(text, "\r")

	return text
}

func printSomething(value interface{}) {
	typedVal, ok := value.(int)

	if ok {
		fmt.Println("integer", typedVal)
		return
	}

	floatVal, ok := value.(float64)

	if ok {
		fmt.Println("float", floatVal)
	}

	stringVal, ok := value.(string)

	if ok {
		fmt.Println("float", stringVal)
	}

}
